from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)

class Technician(models.Model):
    name = models.CharField(max_length=200, unique=True)
    employee_number = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f"{self.name}"

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"employee_number": self.employee_number})

class Appointment(models.Model):
    vin = models.CharField(max_length=200)
    customer_name = models.CharField(max_length=200, unique=False)
    date = models.DateTimeField(null=True)
    service_reason = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name = "appointments",
        on_delete=models.CASCADE,
        null=True,
        blank=False,
    )
    completed = models.BooleanField(default=False)
    vip = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})
