from django.urls import path

from .views import (
    automobilevo,
    automobilevos,
    salesperson,
    salespersons,
    customer,
    customers,
    sales_record,
    sales_records
)

urlpatterns = [
    path("automobilevo/<int:vin>/", automobilevo, name="automobilevo"),
    path("automobilevos/", automobilevos, name="automobilevos"),
    path("salesperson/<int:id>/", salesperson, name="salesperson"),
    path("salespersons/", salespersons, name="salespersons"),
    path("customer/<int:id>/", customer, name="customer"),
    path("customers/", customers, name="customers"),
    path("sale/<int:id>/", sales_record, name="sale"),
    path("sales/", sales_records, name="sales")
]
