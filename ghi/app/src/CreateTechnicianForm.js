import React, {useEffect, useState } from 'react';

function CreateTechnicianForm() {
  const [name, setName] = useState('');
  const [employeeNumber, setEmployeeNumber] = useState('');

  const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.employee_number = employeeNumber;

        const techUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(techUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
        const newTech = await response.json();
        console.log(newTech);

        setName('');
        setEmployeeNumber('');

    }
    }
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleNumberChange = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
    }

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new technician</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="techName" className="form-control" />
                <label htmlFor="name">Technician Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleNumberChange} value={employeeNumber} placeholder="EmployeeNumber" required type="text" name="employeeNumber" id="emmployeeNumber" className="form-control" />
                <label htmlFor="employeeNumber">Employee Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
   );
  }

export default CreateTechnicianForm;
