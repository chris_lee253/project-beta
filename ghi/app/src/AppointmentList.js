import React from 'react';
import { NavLink } from 'react-router-dom';


function AppointmentList({appointment, getAppointment}) {

    const deleteAppointment = async (appt) => {
        const apptUrl = `http://localhost:8080/api/appointments/${appt.id}`;
        const fetchConfig = {
          method: 'delete',
          headers: {
            'Content-Type': 'application/json',
            },
        };
       const response = await fetch(apptUrl, fetchConfig)
       if (response.ok) {
            getAppointment()
       }
    }
/////////////////////////////////////////////////////////////////////////////////////
    const completeAppointment = async (appt) => {
        const apptUrl = `http://localhost:8080/api/appointments/${appt.id}/`;
        const fetchConfig = {
            method: 'put',
            body: JSON.stringify({ completed: true }),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(apptUrl, fetchConfig)
        if (response.ok) {
            getAppointment()
        }
     }

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>VIP Status</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Service Reason</th>
                        <th>Technician</th>
                    </tr>
                </thead>
                <tbody>
                {appointment.map(appt => {
                    if (appt.completed===false)
                    return (
                        <tr key={appt.href}>
                            <td>{ appt.vin }</td>
                            <td>{ appt.customer_name }</td>
                            <td>{ appt.vip ? "Yes" : "No" }</td>
                            <td>{ new Date(appt.date).toLocaleDateString("en-US") }</td>
                            <td>{ new Date(appt.date).toLocaleTimeString([], {hour:"2-digit",minute:"2-digit"})}</td>
                            <td>{ appt.service_reason }</td>
                            <td>{ appt.technician.name }</td>
                            <td>
                                <button className="btn btn-outline-danger" onClick={() => deleteAppointment(appt)}>Cancel</button>
                                <button className="btn btn-outline-success" onClick={() => completeAppointment(appt)}>Completed</button>
                            </td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>

    );
  }

export default AppointmentList;
