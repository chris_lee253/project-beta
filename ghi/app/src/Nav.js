import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Vehicle Information
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/manufacturers">Manufacturers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturers/new">Create a Manufacturer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/model">Vehicle Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="/model/new">Create a Vehicle Model</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobile">Automobiles</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobile/new">Create an Automobile</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales Center
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/salesperson/new">Create a Salesperson</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sale/new">Create a New Sale</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sale/">List of Sales</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salesperson/">Sales History</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customer/new">Create a New Customer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Service Center
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/technicians">Create a Technician</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/new">Create a New Service Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/">Appointments List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/history">Service Appointment History</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>

  )
}

export default Nav;
