import React, {useEffect, useState } from 'react';

function CreateAppointmentForm(props) {
  const [vin, setVin] = useState('');
  const [customerName, setCustomerName] = useState('');
  const [date, setDate] = useState('');
  const [technician, setTechnician] = useState('');
  const [serviceReason, setReason] = useState('');


  const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.vin = vin;
        data.customer_name = customerName;
        data.date = date;
        data.service_reason = serviceReason;
        data.technician = technician;

        const appointmentUrl = `http://localhost:8080/api/appointments/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            console.log(newAppointment);
            setVin('');
            setCustomerName('');
            setDate('');
            setReason('');
            setTechnician('');
            props.getAppointment()

        }
    }
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const handleCustomerNameChange = (event) => {
        const value = event.target.value;
        setCustomerName(value);
    }
    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }
    const handleTechChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }
    const handleServiceChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const [technicians, setTechnicians] = useState([])
    const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians)
    }
  }

  useEffect(() => {
     fetchData();
  }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create Service Appointment</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vinChange">Vin Number</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCustomerNameChange} value={customerName} placeholder="Customer Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="customerNameChange">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDateChange} value={date} placeholder="dateChange" required type="date" name="dateChange" id="dateChange" className="form-control" />
                <label htmlFor="date">Set Appointment Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleServiceChange} value={serviceReason} placeholder="Service Reason" name="reason" id="reason" className="form-control" />
                <label htmlFor="serviceReason">Reason for Service</label>
              </div>
              <div className="mb-3">
                <select onChange={handleTechChange} value={technician} required name="technician" id="technician" className="form-select">
                    <option value="">Choose Technician</option>
                    {technicians.map(tech => {
                        return (
                            <option key={tech.name} value={tech.employee_number}>
                                {tech.name}
                            </option>
                            );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
   );
  }

export default CreateAppointmentForm;
