import React, { useState } from 'react'

export default function SalespersonForm(props) {
    const [name, setName] = useState('')
    const [employeeNumber, setEmployeeNumber] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }
    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
      }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.name = name
        data.employee_number = employeeNumber

        const salespersonUrl = "http://localhost:8090/api/salespersons/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(salespersonUrl, fetchConfig)
        if (response.ok) {
            const newSalesperson = await response.json()
            console.log(newSalesperson)
            setName('')
            setEmployeeNumber('')
            props.getSalespersons()
            props.getSales()
        }
    }


  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add new salesperson</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={employeeNumber} onChange={handleEmployeeNumberChange} placeholder="Employee Number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                <label htmlFor="employee_number">Employee Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  )
}
