import React, {useState, useEffect} from 'react'

export default function SalespersonSalesList(props) {
    const [name, setName] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    return (
        <div>
            <h1>Sales person history</h1>
            <div className="mb-3">
                <select value={name} onChange={handleNameChange} required id="name" name="name" className="form-select">
                    <option value=''>Choose a salesperson</option>
                    {props.salespersons.map(salesperson => {
                        return (
                            <option key={salesperson.id} value={salesperson.name}>
                                {salesperson.name}
                            </option>
                    );
                    })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Sale price</th>
                    </tr>
                </thead>
                <tbody>
                    {props.sales.filter(sale => sale.salesperson.name === name).map(sale => {
                        return (
                        <tr key={sale.id}>
                            <td>{ sale.salesperson.name }</td>
                            <td>{ sale.customer.name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>${ sale.price }</td>
                        </tr>
                        );
                    })}
            </tbody>
            </table>
        </div>
    );
}
