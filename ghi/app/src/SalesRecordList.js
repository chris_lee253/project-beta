import React from 'react'

export default function SalesRecordList(props) {
  return (
    <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Employee Number</th>
                    <th>Customer Name</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {props.sales.map(sale => {
                return (
                    <tr key={sale.id}>
                        <td>{ sale.salesperson.name }</td>
                        <td>{ sale.salesperson.employee_number }</td>
                        <td>{ sale.customer.name }</td>
                        <td>{ sale.automobile.vin }</td>
                        <td>${ sale.price }</td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
    </div>
);
}
